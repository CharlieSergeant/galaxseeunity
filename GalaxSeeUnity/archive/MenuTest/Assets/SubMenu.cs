﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubMenu : MonoBehaviour {

	GameObject openMenu;
	int menuCloseDelay = 5;
	int menuCloseTimer = 0;
	bool triggerClose=false;
	MenuButton [] menuButtons;

	// Use this for initialization
	void Start () {
		openMenu = transform.Find ("Open").gameObject;
		menuButtons = transform.gameObject.GetComponentsInChildren<MenuButton> (true);
	}

	// Update is called once per frame
	void Update () {
		if (menuCloseTimer <= menuCloseDelay)
			menuCloseTimer++;

		if (triggerClose && menuCloseTimer >= menuCloseDelay) {
			Debug.Log ("Close Top Menu");
			triggerClose = false;
			for (int i = 0; i < menuButtons.Length; i++) {
				menuButtons [i].onUp ();
			}
			openMenu.SetActive (false);
		}

		
	}

	public void onEnter() {
		Debug.Log ("Opening Menu");
		openMenu.SetActive (true);
	}

	public void onExit() {
		Debug.Log ("Triggering Close Menu");
		triggerClose = true;
		menuCloseTimer = 0;
	}

	public void onDown() {
		Debug.Log ("SubMenu button pressed");

		triggerClose = true;
		menuCloseTimer = 0;
		for (int i = 0; i < menuButtons.Length; i++) {
			menuButtons [i].onDown ();
		}
		openMenu.SetActive (false);
	}
}
