﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NBodyScript : Integrator {

	int n;
	public double [] x;
	public double [] mass;
	public double shield = 1.0e-3;
	public double G=1.0;
	public double m=1.0;
	public double radius = 1.0;

	public void AllocNBS (int n) {
		this.n = n;
		Init (6 * n);
		x = new double[6 * n];
		mass = new double[n];
	}
		

	// Use this for initialization
	public void InitNBS () {
		for (int i = 0; i < n; i++) {
			bool done = false;
			while (!done) {
				x [i * 6 + 0] = Random.Range (-(float)radius, (float)radius);
				x [i * 6 + 1] = Random.Range (-(float)radius, (float)radius);
				x [i * 6 + 2] = Random.Range (-(float)radius, (float)radius);
				double r2 = x [i * 6 + 0] * x [i * 6 + 0] + 
					x [i * 6 + 1] * x [i * 6 + 1] + 
					x [i * 6 + 2] * x [i * 6 + 2];
				if (r2 < radius * radius)
					done = true;
			}
			mass [i] = m / n;
			x [i * 6 + 3] = 0.0;
			x [i * 6 + 4] = 0.0;
			x [i * 6 + 5] = 0.0;
		}
		// set velocities to go in a "stable" circle
		for (int i = 0; i < n; i++) {
			double r2 = x [i * 6 + 0] * x [i * 6 + 0] + 
				x [i * 6 + 1] * x [i * 6 + 1] + 
				x [i * 6 + 2] * x [i * 6 + 2];
			double r = Mathd.Sqrt (r2);
			double m_interior = m * r / (radius * radius * radius);
			double v = 0.75*Mathd.Sqrt (G * m_interior);
			double th = Mathd.Atan2 (x [i * 6 + 2], x [i * 6 + 0]);
			x [i * 6 + 3] = v * Mathd.Sin (th);
			x [i * 6 + 5] = -v * Mathd.Cos (th);
		}
	}



	// Use this for initialization
	public void InitNBS2 () {
		for (int i = 0; i < n; i++) {
			bool done = false;
			while (!done) {
				x [i * 6 + 0] = Random.Range (-(float)radius, (float)radius);
				x [i * 6 + 1] = Random.Range (-(float)radius, (float)radius);
				x [i * 6 + 2] = Random.Range (-(float)radius, (float)radius);
				double r2 = x [i * 6 + 0] * x [i * 6 + 0] + 
					x [i * 6 + 1] * x [i * 6 + 1] + 
					x [i * 6 + 2] * x [i * 6 + 2];
				if (r2 < radius * radius)
					done = true;
			}
			mass [i] = m / n;
			x [i * 6 + 3] = 0.0;
			x [i * 6 + 4] = 0.0;
			x [i * 6 + 5] = 0.0;
		}
		double pe = getPotentialEnergy ();
		// KE = n * 1/2 * m * v ^2
		//  KE = -1/2 PE
		// v = sqrt( (-1/2 PE) / n * 2 /  m      )
		double v = Mathd.Sqrt(-pe/(m));
		for (int i = 0; i < n; i++) {
			Vector3 v3 = ((float)v)*Random.onUnitSphere;
			x [i * 6 + 3] = v3.x;
			x [i * 6 + 4] = v3.y;
			x [i * 6 + 5] = v3.z;
		}
	}

	public double getKineticEnergy() {
		double ke = 0.0;
		for (int i = 0; i < n; i++) {
			double vx = x [i * 6 + 3];
			double vy = x [i * 6 + 4];
			double vz = x [i * 6 + 5];
			double v = Mathd.Sqrt (vx * vx + vy * vy + vz * vz); 
			ke += 0.5 * mass [i] * v * v;
		}
		return ke;
	}

	public double getPotentialEnergy() {
		double pe = 0.0;
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				double dx = x [j * 6 + 0] - x [i * 6 + 0];
				double dy = x [j * 6 + 1] - x [i * 6 + 1];
				double dz = x [j * 6 + 2] - x [i * 6 + 2];
				double dr = Mathd.Sqrt (dx * dx + dy * dy + dz * dz);
				pe -= G * mass [i] * mass [j] / dr;
			}
		}
		return pe;
	}

	override public void RatesOfChange (double[] x, double[] xdot){
		for (int i = 0; i < n; i++) {
			xdot [i * 6 + 0] = x [i * 6 + 3];
			xdot [i * 6 + 1] = x [i * 6 + 4];
			xdot [i * 6 + 2] = x [i * 6 + 5];
			xdot [i * 6 + 3] = 0.0;
			xdot [i * 6 + 4] = 0.0;
			xdot [i * 6 + 5] = 0.0;
		}
		for (int i = 0; i < n; i++) {
			double xi = x [i * 6 + 0];
			double yi = x [i * 6 + 1];
			double zi = x [i * 6 + 2];
			for (int j = i + 1; j < n; j++) {
				double xj = x [j * 6 + 0];
				double yj = x [j * 6 + 1];
				double zj = x [j * 6 + 2];
				Vector3d drv = new Vector3d (xi - xj, yi - yj, zi - zj);
				double dr2 =  (drv.sqrMagnitude + shield * shield);
				double dr = Mathd.Sqrt (dr2);
				Vector3d accel = -G / dr2 * drv / dr;
				xdot [i * 6 + 3] += accel.x*mass[j];
				xdot [i * 6 + 4] += accel.y*mass[j];
				xdot [i * 6 + 5] += accel.z*mass[j];
				xdot [j * 6 + 3] -= accel.x*mass[i];
				xdot [j * 6 + 4] -= accel.y*mass[i];
				xdot [j * 6 + 5] -= accel.z*mass[i];
			}
		}
	}


}
