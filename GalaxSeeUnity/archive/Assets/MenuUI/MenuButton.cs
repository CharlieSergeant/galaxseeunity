﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuButton : MonoBehaviour {

	bool alert = false;
	public GameObject objectToTrigger;
	public int argument;
	Color alertColor = Color.cyan;
	Color inertColor = Color.gray;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(alert)
			GetComponent<Image> ().color = alertColor;
		else
			GetComponent<Image> ().color = inertColor;
		
	}

	public void onEnter() {
		Debug.Log ("Menu Button Entered");
		alert = true;
	}

	public void onExit() {
		Debug.Log ("Menu Button Exited");
		alert = false;
	}

	public void onUp() {
		if (alert) {
			alert = false;
			string message = string.Format ("Menu Item Activated {0}", gameObject.name);
			objectToTrigger.SendMessage ("onMenuTrigger",argument);
			Debug.Log (message);
		}
	}

	public void onDown() {
		if (alert) {
			Debug.Log ("Foo");

			alert = false;
			string message = string.Format ("Menu Item Activated {0}", gameObject.name);
			objectToTrigger.SendMessage ("onMenuTrigger", argument);
			SubMenu sm = transform.GetComponentInParent<SubMenu> ();
			if (sm != null) {
				sm.onExit ();
			}
			Debug.Log (message);
		}
	}

}
