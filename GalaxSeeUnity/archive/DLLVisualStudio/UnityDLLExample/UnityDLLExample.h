#pragma once

// UnityDLLExample.h : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <math.h>


typedef struct {
	double * x;
	double * store;
	double * k1;
	double * k2;
	double * k3;
	double * k4;
	double * mass;
	double shield;
	double G;
	double m;
	int n;
} Model;

extern "C" __declspec(dllexport) void * createModel(int n);
extern "C" __declspec(dllexport) void stepModelRK2(void * voo, double dt);
extern "C" __declspec(dllexport) void stepModelRK4(void * voo, double dt);
extern "C" __declspec(dllexport) void stepModelEuler(void * voo, double dt);
extern "C" __declspec(dllexport) double getX(void * voo, int i);
extern "C" __declspec(dllexport) double * getXArray(void * voo);
extern "C" __declspec(dllexport) void destroyModel(void * voo);
extern "C" __declspec(dllexport) void ratesOfChange(Model * foo, double* x, double* xdot);
extern "C" __declspec(dllexport) double randRange(double low, double high);
extern "C" __declspec(dllexport) void setX(void * voo, double value, int i);
extern "C" __declspec(dllexport) void setXArray(void * voo, double * value);
