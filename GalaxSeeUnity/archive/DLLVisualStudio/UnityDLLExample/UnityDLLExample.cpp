// UnityDLLExample.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <math.h>
#include "UnityDLLExample.h"

extern "C" __declspec(dllexport)  double randRange(double low, double high) {
	double r = (double)rand() / (double)RAND_MAX;
	return low + r*(high - low);
}

extern "C" __declspec(dllexport) void setShield(void * voo, double shield) {
	Model * foo = (Model *)voo;
	foo->shield = shield;
}

extern "C" __declspec(dllexport) void setMass(void * voo, double m) {
	Model * foo = (Model *)voo;
	foo->m = m;
}

extern "C" __declspec(dllexport) void setMasses(void * voo, double * m) {
	Model * foo = (Model *)voo;
	int n = foo->n;
	foo->m = 0.0;
	for (int i = 0; i < n; i++) {
		foo->mass[i] = m[i];
		foo->m += m[i];
	}
}



extern "C" __declspec(dllexport) void setG(void * voo, double G) {
	Model * foo = (Model *)voo;
	foo->G = G;
}

extern "C" __declspec(dllexport) void * allocModel(int n) {
	Model *foo = (Model *)malloc(sizeof(Model));
	foo->n = n;
	foo->x = (double *)malloc(sizeof(double) * 6 * n);
	foo->store = (double *)malloc(sizeof(double) * 6 * n);
	foo->k1 = (double *)malloc(sizeof(double) * 6 * n);
	foo->k2 = (double *)malloc(sizeof(double) * 6 * n);
	foo->k3 = (double *)malloc(sizeof(double) * 6 * n);
	foo->k4 = (double *)malloc(sizeof(double) * 6 * n);
	foo->mass = (double *)malloc(sizeof(double)*n);

	foo->shield = 1.0e-4;
	foo->G = 1.0;
	foo->m = 1.0;

	return foo;
}

extern "C" __declspec(dllexport) void initModel(void * voo) {
	Model *foo = (Model *)voo;
	int n = foo->n;


	for (int i = 0; i < n; i++) {
		foo->x[i * 6 + 0] = randRange(-1, 1);
		foo->x[i * 6 + 1] = randRange(-1, 1);
		foo->x[i * 6 + 2] = randRange(-1, 1);
		foo->x[i * 6 + 3] = 0.0;
		foo->x[i * 6 + 4] = 0.0;
		foo->x[i * 6 + 5] = 0.0;
		foo->mass[i] = foo->m / (double)n;
	}

}


extern "C" __declspec(dllexport) void stepModelEuler(void * voo, double h) {
	Model * foo = (Model *)voo;

	int nEquations = foo->n*6;

	ratesOfChange(foo, foo->x, foo->k1);
	for (int i = 0; i < nEquations; i++) {
		foo->x[i] = foo->x[i] + foo->k1[i] * h;
	}
}



extern "C" __declspec(dllexport) void stepModelRK4(void * voo, double h) {
	Model * foo = (Model *)voo;

	int nEquations = foo->n*6;

	ratesOfChange(foo,foo->x, foo->k1);
	for (int i = 0; i < nEquations; i++) {
		foo->store[i] = foo->x[i] + foo->k1[i] * h / 2.0;
	}
	ratesOfChange(foo,foo->store, foo->k2);
	for (int i = 0; i < nEquations; i++) {
		foo->store[i] = foo->x[i] + foo->k2[i] * h / 2.0;
	}
	ratesOfChange(foo,foo->store, foo->k3);
	for (int i = 0; i < nEquations; i++) {
		foo->store[i] = foo->x[i] + foo->k3[i] * h;
	}
	ratesOfChange(foo,foo->store, foo->k4);
	for (int i = 0; i < nEquations; i++) {
		foo->x[i] = foo->x[i] + (foo->k1[i] + 2.0*foo->k2[i] + 2.0*foo->k3[i] + foo->k4[i]) * h / 6.0;
	}



}





extern "C" __declspec(dllexport) void stepModelRK2(void * voo, double dt) {
	Model * foo = (Model *)voo;
	for (int i = 0; i < 6*foo->n; i++) {
		foo->store[i] = foo->x[i];
	}
	ratesOfChange(foo, foo->x, foo->k1);
	for (int i = 0; i < 6 * foo->n; i++) {
		foo->x[i] += foo->k1[i] * dt;
	}
	ratesOfChange(foo, foo->x, foo->k2);
	for (int i = 0; i < 6 * foo->n; i++) {
		foo->x[i] = foo->store[i]+0.5*(foo->k1[i]+foo->k2[i]) * dt;
	}
}

extern "C" __declspec(dllexport) void setX(void * voo,double value, int i) {
	Model * foo = (Model *)voo;
	foo->x[i] = value;
}

extern "C" __declspec(dllexport) void setXArray(void * voo, double * value) {
	Model * foo = (Model *)voo;
	int n = foo->n;
	for (int i = 0; i < 6*n; i++) {
		foo->x[i] = value[i];
	}
}

extern "C" __declspec(dllexport) double getX(void * voo, int i) {
	Model * foo = (Model *)voo;
	return foo->x[i];
}

extern "C" __declspec(dllexport) double * getXArray(void * voo) {
	Model * foo = (Model *)voo;
	return foo->x;
}


extern "C" __declspec(dllexport) void destroyModel(void * voo) {
	Model * foo = (Model *)voo;
	free(foo->x);
	free(foo);
}


extern "C" __declspec(dllexport) void ratesOfChange(Model * foo, double* x, double* xdot) {
	int n = foo->n;
	for (int i = 0; i < n; i++) {
		xdot[i * 6 + 0] = x[i * 6 + 3];
		xdot[i * 6 + 1] = x[i * 6 + 4];
		xdot[i * 6 + 2] = x[i * 6 + 5];
		xdot[i * 6 + 3] = 0.0;
		xdot[i * 6 + 4] = 0.0;
		xdot[i * 6 + 5] = 0.0;
	}
	for (int i = 0; i < n; i++) {
		double xi = x[i * 6 + 0];
		double yi = x[i * 6 + 1];
		double zi = x[i * 6 + 2];
		double axi = 0.0;
		double ayi = 0.0;
		double azi = 0.0;
		{
			for (int j = i + 1; j < n; j++) {
				double xj = x[j * 6 + 0];
				double yj = x[j * 6 + 1];
				double zj = x[j * 6 + 2];
				double dx = xi - xj;
				double dy = yi - yj;
				double dz = zi - zj;

				double dr = sqrt(dx*dx + dy*dy + dz*dz + foo->shield*foo->shield);
				double dr2 = dr * dr;
				double dr3 = dr2*dr;
				double accel = -foo->G / dr2;
				axi += accel*dx / dr*foo->mass[j];
				ayi += accel*dy / dr*foo->mass[j];
				azi += accel*dz / dr*foo->mass[j];
				xdot[j * 6 + 3] -= accel*dx / dr*foo->mass[i];
				xdot[j * 6 + 4] -= accel*dy / dr*foo->mass[i];
				xdot[j * 6 + 5] -= accel*dz / dr*foo->mass[i];
			}
			{
				xdot[i * 6 + 3] += axi;
				xdot[i * 6 + 4] += ayi;
				xdot[i * 6 + 5] += azi;
			}
		}
	}
}

