#include "stdafx.h"
#include "UnityDLLExample.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "fcr.h"
#include "octtree_structs.h"
#include "octtree.h"
#ifdef USE_PPPM
#include "pppm_structs.h"
#include "pppm.h"
#endif
#include "nbody.h"
#include <time.h>


extern "C" __declspec(dllexport) void * allocateNbodyModel(int n, int ngrid) {
	NbodyModel * theModel;
	
	theModel = (NbodyModel *)malloc(sizeof(NbodyModel));
	theModel->n = n;
	theModel->mass = (double *)malloc(sizeof(double)*n);
	theModel->x = (double *)malloc(sizeof(double)*n);
	theModel->y = (double *)malloc(sizeof(double)*n);
	theModel->z = (double *)malloc(sizeof(double)*n);
	theModel->draw_x = (double *)malloc(sizeof(double)*n);
	theModel->draw_y = (double *)malloc(sizeof(double)*n);
	theModel->draw_z = (double *)malloc(sizeof(double)*n);
	theModel->srad2 = (double *)malloc(sizeof(double)*n);
	theModel->vx = (double *)malloc(sizeof(double)*n);
	theModel->vy = (double *)malloc(sizeof(double)*n);
	theModel->vz = (double *)malloc(sizeof(double)*n);
	theModel->X = (double *)malloc(sizeof(double)*n * 6);
	theModel->X2 = (double *)malloc(sizeof(double)*n * 6);
	theModel->color3 = (double *)malloc(sizeof(double)*n * 3);
	theModel->XPRIME = (double *)malloc(sizeof(double)*n * 6);
	theModel->XPRIME1 = (double *)malloc(sizeof(double)*n * 6);
	theModel->XPRIME2 = (double *)malloc(sizeof(double)*n * 6);
	theModel->XPRIME3 = (double *)malloc(sizeof(double)*n * 6);
	theModel->XPRIME4 = (double *)malloc(sizeof(double)*n * 6);
	theModel->rootTree = allocOctTree();
	(theModel->rootTree)->nodes = NULL;
	(theModel->rootTree)->n_stars_contained = 0;
	theModel->treeList = (OctTree **)malloc(sizeof(OctTree *)*n);
#ifdef USE_PPPM
	theModel->thePPPM = (PPPM *)malloc(sizeof(PPPM));
	allocPPPM(theModel->thePPPM, ngrid, ngrid, ngrid);
#endif
	theModel->tstep_last = -1.0;
	theModel->force_total = 0.0;
	theModel->pointsize = 0.02;
}

extern "C" __declspec(dllexport) void setPointsizeNbodyModel(NbodyModel *theModel, double pointsize) {
	theModel->pointsize = pointsize;
}
extern "C" __declspec(dllexport) void setAnisotropyNbodyModel(NbodyModel *theModel, double anisotropy) {
	theModel->anisotropy = anisotropy;
}
extern "C" __declspec(dllexport) void setDistributionNbodyModel(NbodyModel *theModel, int distribution) {
	theModel->distribution = distribution;
}
extern "C" __declspec(dllexport)void setPPPMCoeffsNbodyModel(NbodyModel *theModel, double ksigma, double knear) {
	theModel->pppm_ksigma = ksigma;
	theModel->pppm_knear = knear;
}
extern "C" __declspec(dllexport)void setExpansionNbodyModel(NbodyModel *theModel, double expansion) {
	theModel->expansion = expansion;
}
extern "C" __declspec(dllexport)void setDragNbodyModel(NbodyModel *theModel, double drag) {
	theModel->drag = drag;
}
/*extern "C" __declspec(dllexport)void setPrefixNbodyModel(NbodyModel *theModel, const char * prefix) {
	strcpy(theModel->prefix, prefix);
}*/

extern "C" __declspec(dllexport) void setDefaultsNbodyModel(NbodyModel *theModel) {
	theModel->default_mass = 1.0; // solar masses
	theModel->default_scale = 10.0; // parsecs
	theModel->default_G = 0.0046254; // pc^2/Solar_mass/My^2
	theModel->srad_factor = 5.0;
	theModel->softening_factor = 0.0;
	theModel->rotation_factor = 0.0;
	theModel->initial_v = 0.0;
	theModel->tFinal = 1000.0;
	theModel->tstep = 1.0;
	theModel->int_method = INT_METHOD_RK4;
	theModel->int_method = FORCE_METHOD_DIRECT;
	theModel->treeRangeCoefficient = 1.2;
	theModel->drag = 0.0;
	theModel->expansion = 0.0;
	theModel->distribution = DISTRIBUTION_SPHERICAL_RANDOM;
	//strcpy(theModel->prefix, "out");
}
extern "C" __declspec(dllexport)void setForceMethod(NbodyModel *theModel, int force_method) {
	theModel->force_method = force_method;
}
extern "C" __declspec(dllexport)void setTreeRangeCoefficient(NbodyModel *theModel, double coefficient) {
	theModel->treeRangeCoefficient = coefficient;
}
extern "C" __declspec(dllexport)void setIntMethod(NbodyModel *theModel, int int_method) {
	theModel->int_method = int_method;
}
extern "C" __declspec(dllexport)void setTStep(NbodyModel *theModel, double tstep) {
	theModel->tstep = tstep;
}
extern "C" __declspec(dllexport)void setRotationFactor(NbodyModel *theModel, double rotation_factor) {
	theModel->rotation_factor = rotation_factor;
}
extern "C" __declspec(dllexport)void setTFinal(NbodyModel *theModel, double tFinal) {
	theModel->tFinal = tFinal;
}
extern "C" __declspec(dllexport)void setInitialV(NbodyModel *theModel, double initial_v) {
	theModel->initial_v = initial_v;
}
extern "C" __declspec(dllexport)void setSofteningNbodyModel(NbodyModel *theModel, double softening_factor) {
	theModel->softening_factor = softening_factor;
}
extern "C" __declspec(dllexport)void setSradNbodyModel(NbodyModel *theModel, double srad_factor) {
	theModel->srad_factor = srad_factor;
}
extern "C" __declspec(dllexport)void setMassNbodyModel(NbodyModel *theModel, double mass) {
	theModel->default_mass = mass;
}
extern "C" __declspec(dllexport)void setScaleNbodyModel(NbodyModel *theModel, double scale) {
	theModel->default_scale = scale;
}
extern "C" __declspec(dllexport)void setGNbodyModel(NbodyModel *theModel, double G) {
	theModel->default_G = G;
}

extern "C" __declspec(dllexport) void initializeNbodyModel(NbodyModel *theModel) {
	int i, j, k, l;
	int itest;
	double r2;
	double scale = theModel->default_scale;
	double r2Max = scale * scale;
	double tstep_squared, srad;
	double x, y, z, xmin, ymin, zmin, xstep, ystep, zstep;
	for (i = 0; i<theModel->n; i++) {
		theModel->mass[i] = theModel->default_mass;
		if (theModel->distribution == DISTRIBUTION_SPHERICAL_RANDOM) {
			do {
				theModel->x[i] = scale * (2.0*(double)rand() /
					(double)RAND_MAX - 1.0);
				theModel->y[i] = scale * (2.0*(double)rand() /
					(double)RAND_MAX - 1.0);
				theModel->z[i] = scale * (2.0*(double)rand() /
					(double)RAND_MAX - 1.0);
				r2 = theModel->x[i] * theModel->x[i] +
					theModel->y[i] * theModel->y[i] +
					theModel->z[i] * theModel->z[i];
			} while (r2>r2Max);
		}
		else if (theModel->distribution == DISTRIBUTION_RECTANGULAR_RANDOM) {
			//do {
			theModel->x[i] = scale * (2.0*(double)rand() /
				(double)RAND_MAX - 1.0);
			theModel->y[i] = scale * (2.0*(double)rand() /
				(double)RAND_MAX - 1.0);
			theModel->z[i] = scale * (2.0*(double)rand() /
				(double)RAND_MAX - 1.0);
			r2 = theModel->x[i] * theModel->x[i] +
				theModel->y[i] * theModel->y[i] +
				theModel->z[i] * theModel->z[i];
			//} while (r2<r2Max) ;
		}
		else if (theModel->distribution == DISTRIBUTION_RECTANGULAR_UNIFORM) {
			itest = (int)pow((double)theModel->n + 1.0, (1.0 / 3.0));
			if (itest*itest*itest != theModel->n) {
				printf("WARNING: RECTANGULAR UNIFORM DIST REQUIRES\n");
				printf("    N BE CUBIC NUMBER\n");
				printf("    %d    %d  \n", itest, theModel->n);
				exit(0);
			}
			xstep = (2.0*theModel->default_scale) / itest;
			ystep = (2.0*theModel->default_scale) / itest;
			zstep = (2.0*theModel->default_scale) / itest;
			xmin = -theModel->default_scale + xstep / 2.0;
			ymin = -theModel->default_scale + ystep / 2.0;
			zmin = -theModel->default_scale + zstep / 2.0;
			for (l = 0; l<itest; l++) {
				x = xmin + l * xstep;
				for (j = 0; j<itest; j++) {
					y = ymin + j * ystep;
					for (k = 0; k<itest; k++) {
						z = zmin + k * zstep;
						theModel->x[l*itest*itest + j * itest + k] = x +
							drand(-1.0, 1.0)*theModel->anisotropy;
						theModel->y[l*itest*itest + j * itest + k] = y +
							drand(-1.0, 1.0)*theModel->anisotropy;
						theModel->z[l*itest*itest + j * itest + k] = z +
							drand(-1.0, 1.0)*theModel->anisotropy;
					}
				}
			}
		}
		else {
			printf("WARNING: Distribution not understood\n");
			exit(0);
		}
		theModel->vx[i] = 0.0;
		theModel->vy[i] = 0.0;
		theModel->vz[i] = 0.0;
		for (j = 0; j<6; j++) {
			theModel->X[i * 6 + j] = 0.0;
			theModel->XPRIME[i * 6 + j] = 0.0;
		}
		for (j = 0; j<3; j++) {
			theModel->color3[i * 3 + j] = 1.0;
		}
	}
	theModel->G = theModel->default_G;
	theModel->t = 0.0;
	theModel->iteration = 0;
	theModel->abmCounter = -3;
	theModel->bhCounter = 0;
	tstep_squared = theModel->tstep*theModel->tstep;
	for (i = 0; i<theModel->n; i++) {
		srad = computeSoftenedRadius(theModel->G*theModel->mass[i],
			tstep_squared, theModel->srad_factor);
		theModel->srad2[i] = srad * srad;
	}
	calcStatistics(theModel);
	/*
	for(i=0;i<theModel->n;i++) {
	theModel->x[i] -= theModel->comx;
	theModel->y[i] -= theModel->comy;
	theModel->z[i] -= theModel->comz;
	theModel->vx[i] -= theModel->copx;
	theModel->vy[i] -= theModel->copy;
	theModel->vz[i] -= theModel->copz;
	}
	*/
	populateOctTree(theModel->rootTree, theModel->treeList, theModel,
		0,
		NULL,
		theModel->n,
		(-theModel->default_scale),
		theModel->default_scale,
		-(theModel->default_scale),
		theModel->default_scale,
		-(theModel->default_scale),
		theModel->default_scale);
}

extern "C" __declspec(dllexport) void freeNbodyModel(NbodyModel *theModel) {
	free(theModel->x);
	free(theModel->y);
	free(theModel->z);
	free(theModel->mass);
	free(theModel->vx);
	free(theModel->vy);
	free(theModel->vz);
	free(theModel->srad2);
	free(theModel->X);
	free(theModel->X2);
	free(theModel->draw_x);
	free(theModel->draw_y);
	free(theModel->draw_z);
	free(theModel->color3);
	free(theModel->XPRIME);
	free(theModel->XPRIME1);
	free(theModel->XPRIME2);
	free(theModel->XPRIME3);
	free(theModel->XPRIME4);
	freeOctTree(theModel->rootTree, 0);
	free(theModel->treeList);
#ifdef USE_PPPM
	freePPPM(theModel->thePPPM);
	//free(theModel->thePPPM);
#endif
#ifdef HAS_FFTW3
	fftw_cleanup();
#endif
	free(theModel);
}

extern "C" __declspec(dllexport) void calcDerivs(double * x, double * derivs, double t, double tStep,
	NbodyModel * theModel) {

	time(&theModel->force_begin);
	switch (theModel->force_method) {
#ifdef USE_PPPM
	case FORCE_METHOD_PPPM:
		calcDerivsPPPM(x, derivs, t, tStep, theModel);
		break;
#endif
	case FORCE_METHOD_TREE:
		calcDerivsBarnesHut(x, derivs, t, tStep, theModel);
		break;
	case FORCE_METHOD_DIRECT:
		calcDerivsDirect(x, derivs, t, tStep, theModel);
		break;
	}
	time(&theModel->force_end);
	theModel->force_total += difftime(theModel->force_end,
		theModel->force_begin);
}

#ifdef USE_PPPM
extern "C" __declspec(dllexport) void calcDerivsPPPM(double * x, double * derivs, double t, double tStep,
	NbodyModel * theModel) {
	int i, j;
	point3d accel;

	double xmax = theModel->default_scale;
	double xmin = -xmax;
	double ymax = xmax;
	double ymin = xmin;
	double zmax = xmax;
	double zmin = xmin;
	double sigma = theModel->pppm_ksigma*(xmax - xmin) / theModel->thePPPM->nx;
	double near = theModel->pppm_knear*(xmax - xmin) / theModel->thePPPM->nx;

	//enforce periodic boundaries, ensure that any objects are within range
	for (i = 0; i<theModel->n; i++) {
		for (j = 0; j<3; j++) {
			while (x[6 * i + j]<-theModel->default_scale)
				x[6 * i + j] += theModel->default_scale*2.0;
			while (x[6 * i + j]>theModel->default_scale)
				x[6 * i + j] -= theModel->default_scale*2.0;
		}
	}

	//interpolate masses onto density distribution
	populateDensityPPPM(theModel->thePPPM, theModel, x, xmin, xmax, ymin, ymax, zmin, zmax, sigma);
	prepPotentialPPPM(theModel->thePPPM, theModel);
	//for each object, calculate forces
	for (i = 0; i<theModel->n; i++) {
		derivs[i * 6 + 0] = x[i * 6 + 3];
		derivs[i * 6 + 1] = x[i * 6 + 4];
		derivs[i * 6 + 2] = x[i * 6 + 5];
		near = 0.0;
		accel = calculateForcePPPM(i, x[i * 6], x[i * 6 + 1], x[i * 6 + 2],
			theModel->thePPPM, theModel, x, near);
		derivs[i * 6 + 3] = accel.x - theModel->drag*x[i * 6 + 3] / theModel->mass[i];
		derivs[i * 6 + 4] = accel.y - theModel->drag*x[i * 6 + 4] / theModel->mass[i];
		derivs[i * 6 + 5] = accel.z - theModel->drag*x[i * 6 + 5] / theModel->mass[i];
	}
}
#endif

extern "C" __declspec(dllexport) void calcDerivsBarnesHut(double * x, double * derivs, double t, double tStep,
	NbodyModel * theModel) {
	int i;
	point3d accel;
	for (i = 0; i<theModel->n; i++) {
		if (theModel->treeList[i] != NULL) {
			(theModel->treeList[i])->com_x = x[i * 6];
			(theModel->treeList[i])->com_y = x[i * 6 + 1];
			(theModel->treeList[i])->com_z = x[i * 6 + 2];
		}
	}
	resetOctTree(theModel->rootTree, theModel);
	for (i = 0; i<theModel->n; i++) {
		derivs[i * 6 + 0] = x[i * 6 + 3];
		derivs[i * 6 + 1] = x[i * 6 + 4];
		derivs[i * 6 + 2] = x[i * 6 + 5];
		accel = calculateForceOctTree(1, i, x[i * 6], x[i * 6 + 1], x[i * 6 + 2],
			theModel->G, theModel->rootTree, theModel->softening_factor);
		derivs[i * 6 + 3] = accel.x - theModel->drag*x[i * 6 + 3] / theModel->mass[i];
		derivs[i * 6 + 4] = accel.y - theModel->drag*x[i * 6 + 4] / theModel->mass[i];
		derivs[i * 6 + 5] = accel.z - theModel->drag*x[i * 6 + 5] / theModel->mass[i];
	}
}

extern "C" __declspec(dllexport) void calcDerivsDirect(double * x, double * derivs, double t, double tStep,
	NbodyModel * theModel) {
	int i, j;
	double r3i;
	double deltaX, deltaY, deltaZ;
	double rad, r2;

	// calculate a
	for (i = 0; i < theModel->n; i++) {
		derivs[i * 6 + 0] = x[i * 6 + 3];
		derivs[i * 6 + 1] = x[i * 6 + 4];
		derivs[i * 6 + 2] = x[i * 6 + 5];
		derivs[i * 6 + 3] = 0.0;
		derivs[i * 6 + 4] = 0.0;
		derivs[i * 6 + 5] = 0.0;

		for (j = 0; j < i; j++) {
			deltaX = x[j * 6 + 0] - x[i * 6 + 0];
			deltaY = x[j * 6 + 1] - x[i * 6 + 1];
			deltaZ = x[j * 6 + 2] - x[i * 6 + 2];
			r2 = deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ +
				theModel->softening_factor*theModel->softening_factor;
			rad = sqrt(r2);
			r3i = theModel->G / (rad*r2);
			if (r2 > theModel->srad2[j]) {
				derivs[i * 6 + 3] += theModel->mass[j] * deltaX*r3i;
				derivs[i * 6 + 4] += theModel->mass[j] * deltaY*r3i;
				derivs[i * 6 + 5] += theModel->mass[j] * deltaZ*r3i;
			}
			if (r2 > theModel->srad2[i]) {
				derivs[j * 6 + 3] -= theModel->mass[i] * deltaX*r3i;
				derivs[j * 6 + 4] -= theModel->mass[i] * deltaY*r3i;
				derivs[j * 6 + 5] -= theModel->mass[i] * deltaZ*r3i;
			}
		}
		for (i = 0; i < theModel->n; i++) {
			derivs[i * 6 + 3] -= theModel->drag*x[i * 6 + 3] / theModel->mass[i];
			derivs[i * 6 + 4] -= theModel->drag*x[i * 6 + 4] / theModel->mass[i];
			derivs[i * 6 + 5] -= theModel->drag*x[i * 6 + 5] / theModel->mass[i];
		}
	}
}

void copy2X(NbodyModel * theModel)
{
	int i;
	for (i = 0; i<theModel->n; i++) {
		theModel->X[i * 6 + 0] = theModel->x[i];
		theModel->X[i * 6 + 1] = theModel->y[i];
		theModel->X[i * 6 + 2] = theModel->z[i];
		theModel->X[i * 6 + 3] = theModel->vx[i];
		theModel->X[i * 6 + 4] = theModel->vy[i];
		theModel->X[i * 6 + 5] = theModel->vz[i];
	}
}
void copy2xyz(NbodyModel * theModel) {
	int i;
	for (i = 0; i<theModel->n; i++) {
		theModel->x[i] = theModel->X[i * 6 + 0];
		theModel->y[i] = theModel->X[i * 6 + 1];
		theModel->z[i] = theModel->X[i * 6 + 2];
		theModel->vx[i] = theModel->X[i * 6 + 3];
		theModel->vy[i] = theModel->X[i * 6 + 4];
		theModel->vz[i] = theModel->X[i * 6 + 5];
	}
}
extern "C" __declspec(dllexport) void stepNbodyModel(NbodyModel * theModel) {
	int treeSkip = 3;
	double treeScaleFactor = 2.0;
	int i;
	double srad;
	int success;
	double tstep_squared;
	double rmsqdist, scale;
	double factor;
	if (theModel->tstep != theModel->tstep_last) {
		tstep_squared = theModel->tstep*theModel->tstep;
		for (i = 0; i<theModel->n; i++) {
			srad = computeSoftenedRadius(theModel->G*theModel->mass[i],
				tstep_squared, theModel->srad_factor);
			theModel->srad2[i] = srad * srad;
		}
		// flag to repopulate the tree, reset the repop counter
		theModel->tstep_last = theModel->tstep;
		theModel->abmCounter = -3;
		theModel->bhCounter = 0;
	}
	if (theModel->bhCounter == 0 || theModel->rootTree == NULL) {
		freeOctTree(theModel->rootTree, 1);
		scale = theModel->default_scale*treeScaleFactor;
		rmsqdist = 0.0;
		for (i = 0; i<theModel->n; i++) {
			theModel->treeList[i] = NULL;
			rmsqdist += theModel->x[i] * theModel->x[i] +
				theModel->y[i] * theModel->y[i] +
				theModel->z[i] * theModel->z[i];
		}
		rmsqdist /= (double)theModel->n;
		rmsqdist = sqrt(rmsqdist);
		if (rmsqdist>(theModel->default_scale)) scale = rmsqdist * treeScaleFactor;
		populateOctTree(theModel->rootTree, theModel->treeList, theModel,
			0, NULL, theModel->n, -scale, scale, -scale, scale, -scale, scale);
	}
	else {
		/*
		// this is already done in calc derivs no matter what
		for(i=0;i<theModel->n;i++) {
		if(theModel->treeList[i]!=NULL) {
		theModel->treeList[i]->com_x=theModel->x[i];
		theModel->treeList[i]->com_y=theModel->y[i];
		theModel->treeList[i]->com_z=theModel->z[i];
		}
		}
		resetOctTree(theModel->rootTree,theModel);
		*/
	}
	theModel->bhCounter = (theModel->bhCounter + 1) % treeSkip;
	switch (theModel->int_method) {
	case INT_METHOD_ABM:
		stepNbodyModelABM(theModel, theModel->tstep);
		break;
	case INT_METHOD_RK4:
		stepNbodyModelRK4(theModel, theModel->tstep);
		break;
	case INT_METHOD_LEAPFROG:
		stepNbodyModelLeapfrog(theModel, theModel->tstep);
		break;
	case INT_METHOD_MPEULER:
		stepNbodyModelMPEuler(theModel, theModel->tstep);
		break;
	case INT_METHOD_IEULER:
		stepNbodyModelIEuler(theModel, theModel->tstep);
		break;
	case INT_METHOD_EULER:
	default:
		stepNbodyModelEuler(theModel, theModel->tstep);
		break;
	}
	//update model for periodic boundary conditions
	if (theModel->force_method == FORCE_METHOD_PPPM) {
		for (i = 0; i<theModel->n; i++) {
			while (theModel->x[i]<-theModel->default_scale)
				theModel->x[i] += 2.0*theModel->default_scale;
			while (theModel->y[i]<-theModel->default_scale)
				theModel->y[i] += 2.0*theModel->default_scale;
			while (theModel->z[i]<-theModel->default_scale)
				theModel->z[i] += 2.0*theModel->default_scale;
			while (theModel->x[i]>theModel->default_scale)
				theModel->x[i] -= 2.0*theModel->default_scale;
			while (theModel->y[i]>theModel->default_scale)
				theModel->y[i] -= 2.0*theModel->default_scale;
			while (theModel->z[i]>theModel->default_scale)
				theModel->z[i] -= 2.0*theModel->default_scale;
		}
	}

	for (i = 0; i<theModel->n; i++) {
		theModel->draw_x[i] = theModel->x[i];
		theModel->draw_y[i] = theModel->y[i];
		theModel->draw_z[i] = theModel->z[i];
	}

	// expand universe (not sure this works with ABM method)
	factor = (theModel->default_scale + theModel->expansion*theModel->tstep) /
		theModel->default_scale;
	theModel->default_scale += theModel->expansion*theModel->tstep;
	for (i = 0; i < theModel->n; i++) {
		theModel->x[i] *= factor;
		theModel->y[i] *= factor;
		theModel->z[i] *= factor;
	}
}
extern "C" __declspec(dllexport) void stepNbodyModelIEuler(NbodyModel * theModel, double tStep) {
	int i;

	copy2X(theModel);

	calcDerivs(theModel->X, theModel->XPRIME, theModel->t, tStep, theModel);
	// update v,x
	for (i = 0; i<theModel->n * 6; i++) {
		theModel->X2[i] = theModel->X[i] + theModel->XPRIME[i] * tStep;
	}
	calcDerivs(theModel->X2, theModel->XPRIME2, theModel->t, tStep, theModel);
	for (i = 0; i<theModel->n * 6; i++) {
		theModel->XPRIME[i] = (theModel->XPRIME[i] + theModel->XPRIME2[i]) / 2;
	}
	for (i = 0; i<theModel->n * 6; i++) {
		theModel->X[i] = theModel->X[i] + theModel->XPRIME[i] * tStep;
	}

	copy2xyz(theModel);
	theModel->t += tStep;
	theModel->iteration++;
}
extern "C" __declspec(dllexport) void stepNbodyModelMPEuler(NbodyModel * theModel, double tStep) {
	int i;

	copy2X(theModel);

	calcDerivs(theModel->X, theModel->XPRIME, theModel->t, tStep, theModel);

	// update v,x
	for (i = 0; i<theModel->n * 6; i++) {
		theModel->X2[i] = theModel->X[i] + theModel->XPRIME[i] * tStep / 2;
	}

	calcDerivs(theModel->X2, theModel->XPRIME, theModel->t, tStep, theModel);

	for (i = 0; i<theModel->n * 6; i++) {
		theModel->X[i] = theModel->X[i] + theModel->XPRIME[i] * tStep;
	}

	copy2xyz(theModel);
	theModel->t += tStep;
	theModel->iteration++;
}
extern "C" __declspec(dllexport) void stepNbodyModelRK4(NbodyModel * theModel, double tStep) {
	int i;

	copy2X(theModel);

	calcDerivs(theModel->X, theModel->XPRIME, theModel->t, tStep, theModel);

	// update v,x
	for (i = 0; i<theModel->n * 6; i++) {
		theModel->X2[i] = theModel->X[i] + theModel->XPRIME[i] * tStep / 2;
	}
	calcDerivs(theModel->X2, theModel->XPRIME2, theModel->t + tStep / 2, tStep, theModel);
	for (i = 0; i<theModel->n * 6; i++) {
		theModel->X2[i] = theModel->X[i] + theModel->XPRIME2[i] * tStep / 2;
	}
	calcDerivs(theModel->X2, theModel->XPRIME3, theModel->t + tStep / 2, tStep, theModel);
	for (i = 0; i<theModel->n * 6; i++) {
		theModel->X2[i] = theModel->X[i] + theModel->XPRIME3[i] * tStep;
	}
	calcDerivs(theModel->X2, theModel->XPRIME4, theModel->t + tStep, tStep, theModel);
	for (i = 0; i<theModel->n * 6; i++) {
		theModel->XPRIME[i] += 2.0*theModel->XPRIME2[i] +
			2.0*theModel->XPRIME3[i] +
			theModel->XPRIME4[i];
		theModel->XPRIME[i] /= 6.0;
	}
	for (i = 0; i<theModel->n * 6; i++) {
		theModel->X[i] += theModel->XPRIME[i] * tStep;
	}

	copy2xyz(theModel);
	theModel->t += tStep;
	theModel->iteration++;
}
extern "C" __declspec(dllexport) void stepNbodyModelLeapfrog(NbodyModel * theModel, double tStep) {
	int i;

	copy2X(theModel);

	calcDerivs(theModel->X, theModel->XPRIME, theModel->t, tStep, theModel);

	if (theModel->t<0.5*tStep) {
		// setup leapfrog on first step, change velocities by a half step
		for (i = 0; i<theModel->n; i++) {
			theModel->X[i * 6 + 3] += 0.5*theModel->XPRIME[i * 6 + 3] * tStep;
			theModel->X[i * 6 + 4] += 0.5*theModel->XPRIME[i * 6 + 4] * tStep;
			theModel->X[i * 6 + 5] += 0.5*theModel->XPRIME[i * 6 + 5] * tStep;
		}
	}
	else {
		// update v,x
		for (i = 0; i<theModel->n * 6; i++) {
			theModel->X[i] += theModel->XPRIME[i] * tStep;
		}
	}


	copy2xyz(theModel);
	theModel->t += tStep;
	theModel->iteration++;
}
extern "C" __declspec(dllexport) void stepNbodyModelEuler(NbodyModel * theModel, double tStep) {
	int i;

	copy2X(theModel);

	calcDerivs(theModel->X, theModel->XPRIME, theModel->t, tStep, theModel);

	// update v,x
	for (i = 0; i<theModel->n * 6; i++) {
		theModel->X[i] += theModel->XPRIME[i] * tStep;
	}

	copy2xyz(theModel);
	theModel->t += tStep;
	theModel->iteration++;
}
extern "C" __declspec(dllexport) void stepNbodyModelABM(NbodyModel * theModel, double tStep) {
	//Adams-Bashforth-Moulton Predictor Corrector
	int i;
	double * fk3 = NULL;
	double * fk2 = NULL;
	double * fk1 = NULL;
	double * fk0 = NULL;
	double * fkp = NULL;

	// determine if previous steps exist, if not, populate w/ RK4
	if (theModel->abmCounter<0) {
		stepNbodyModelRK4(theModel, tStep);
		if (theModel->abmCounter == -3) {
			for (i = 0; i<theModel->n * 6; i++)
				theModel->XPRIME4[i] = theModel->XPRIME[i];
		}
		else if (theModel->abmCounter == -2) {
			for (i = 0; i<theModel->n * 6; i++)
				theModel->XPRIME3[i] = theModel->XPRIME[i];
		}
		else {
			for (i = 0; i<theModel->n * 6; i++)
				theModel->XPRIME2[i] = theModel->XPRIME[i];
		}
	}
	else {
		copy2X(theModel);
		if (theModel->abmCounter % 5 == 0) {
			fk3 = theModel->XPRIME4;
			fk2 = theModel->XPRIME3;
			fk1 = theModel->XPRIME2;
			fk0 = theModel->XPRIME1;
			fkp = theModel->XPRIME;
		}
		else if (theModel->abmCounter % 5 == 1) {
			fk3 = theModel->XPRIME3;
			fk2 = theModel->XPRIME2;
			fk1 = theModel->XPRIME1;
			fk0 = theModel->XPRIME;
			fkp = theModel->XPRIME4;
		}
		else if (theModel->abmCounter % 5 == 2) {
			fk3 = theModel->XPRIME2;
			fk2 = theModel->XPRIME1;
			fk1 = theModel->XPRIME;
			fk0 = theModel->XPRIME4;
			fkp = theModel->XPRIME3;
		}
		else if (theModel->abmCounter % 5 == 3) {
			fk3 = theModel->XPRIME1;
			fk2 = theModel->XPRIME;
			fk1 = theModel->XPRIME4;
			fk0 = theModel->XPRIME3;
			fkp = theModel->XPRIME2;
		}
		else if (theModel->abmCounter % 5 == 4) {
			fk3 = theModel->XPRIME;
			fk2 = theModel->XPRIME4;
			fk1 = theModel->XPRIME3;
			fk0 = theModel->XPRIME2;
			fkp = theModel->XPRIME1;
		}
		calcDerivs(theModel->X, fk0, theModel->t, tStep, theModel);
		for (i = 0; i<theModel->n * 6; i++) {
			theModel->X2[i] = theModel->X[i] +
				(tStep / 24.0)*(-9.0*fk3[i] + 37.0*fk2[i]
					- 59.0*fk1[i] + 55.0*fk0[i]);
		}
		calcDerivs(theModel->X2, fkp, theModel->t + tStep, tStep, theModel);
		for (i = 0; i<theModel->n * 6; i++) {
			theModel->X[i] = theModel->X[i] +
				(tStep / 24.0)*(fk2[i] - 5.0*fk1[i] +
					19.0*fk0[i] + 9.0*fkp[i]);
		}
		copy2xyz(theModel);
		theModel->t += tStep;
		theModel->iteration++;
	}

	theModel->abmCounter++;
}

extern "C" __declspec(dllexport) void setX(NbodyModel *theModel, double value, int i) {
	theModel->x[i] = value;
}

extern "C" __declspec(dllexport) void setXArray(NbodyModel *theModel, double * value) {

	int n = theModel->n;
	for (int i = 0; i < 6 * n; i++) {
		theModel->x[i] = value[i];
	}
}

extern "C" __declspec(dllexport) double getX(NbodyModel *theModel, int i) {
	return theModel->x[i];
}

extern "C" __declspec(dllexport) double * getXArray(NbodyModel *theModel) {
	return theModel->x;
}

extern "C" __declspec(dllexport) void speedNbodyModel(NbodyModel *theModel) {
	double pi = atan(1.0)*4.0;
	double theta, phi;
	double v = theModel->initial_v;
	int i;
	for (i = 0; i<theModel->n; i++) {
		theta = 2 * pi*(double)rand() / (double)RAND_MAX;
		phi = pi * (double)rand() / (double)RAND_MAX;
		theModel->vx[i] += v * sin(phi)*cos(theta);
		theModel->vy[i] += v * sin(phi)*sin(theta);
		theModel->vz[i] += v * cos(phi);
	}
}

extern "C" __declspec(dllexport) void spinNbodyModel(NbodyModel *theModel) {
	int i, j;
	double ri, rj, sumM, vTan;
	double rotFactor = theModel->rotation_factor;

	for (i = 0; i<theModel->n; i++) {
		ri = sqrt(pow(theModel->x[i], 2.0) + pow(theModel->y[i], 2.0));
		sumM = 0.0;
		for (j = 0; j<theModel->n; j++) {
			if (i != j) {
				rj = sqrt(pow(theModel->x[j], 2.0) + pow(theModel->y[j], 2.0));
				if (rj<ri) {
					sumM += theModel->mass[j];
				}
			}
		}
		if (sumM>0.0) {
			vTan = sqrt(sumM*theModel->G / ri);
			theModel->vz[i] += 0.0;
			theModel->vx[i] += -rotFactor * vTan*theModel->y[i] / ri;
			theModel->vy[i] += rotFactor * vTan*theModel->x[i] / ri;
		}
	}
	return;
}

extern "C" __declspec(dllexport) void printStatistics(NbodyModel * theModel) {
	double total_energy;
	calcStatistics(theModel);

	total_energy = theModel->KE + theModel->PE;
	printf("KE = %le \t", theModel->KE);
	printf("PE = %le \t", theModel->PE);
	printf("TE = %le \n", total_energy);
	printf("COM x = %le (%10.3le) \t COP x = %le (%10.3le) \n", theModel->comx, theModel->rmsd, theModel->copx, theModel->rmsp);
	printf("COM y = %le \t COP y = %le \n", theModel->comy, theModel->copy);
	printf("COM z = %le \t COP z = %le \n", theModel->comz, theModel->copz);
}
extern "C" __declspec(dllexport) void calcStatistics(NbodyModel * theModel) {
	int i, j;
	double v2, r2, r, dx, dy, dz;
	double total_mass;

	theModel->KE = 0.0;
	theModel->PE = 0.0;
	theModel->comx = 0.0;
	theModel->comy = 0.0;
	theModel->comz = 0.0;
	theModel->copx = 0.0;
	theModel->copy = 0.0;
	theModel->copz = 0.0;
	theModel->rmsd = 0.0;
	theModel->rmsp = 0.0;

	total_mass = 0.0;
	for (i = 0; i<theModel->n; i++) {
		r2 = theModel->x[i] * theModel->x[i] +
			theModel->y[i] * theModel->y[i] +
			theModel->z[i] * theModel->z[i];
		v2 = theModel->vx[i] * theModel->vx[i] +
			theModel->vy[i] * theModel->vy[i] +
			theModel->vz[i] * theModel->vz[i];
		theModel->KE += 0.5*theModel->mass[i] * v2;
		theModel->comx += theModel->mass[i] * theModel->x[i];
		theModel->comy += theModel->mass[i] * theModel->y[i];
		theModel->comz += theModel->mass[i] * theModel->z[i];
		theModel->copx += theModel->mass[i] * theModel->vx[i];
		theModel->copy += theModel->mass[i] * theModel->vy[i];
		theModel->copz += theModel->mass[i] * theModel->vz[i];
		theModel->rmsd += r2;
		theModel->rmsp += v2 * theModel->mass[i] * theModel->mass[i];
		total_mass += theModel->mass[i];
	}
	theModel->comx /= total_mass;
	theModel->comy /= total_mass;
	theModel->comz /= total_mass;
	theModel->rmsd = sqrt(theModel->rmsd / theModel->n);
	theModel->rmsp = sqrt(theModel->rmsp / theModel->n);
	theModel->copx /= theModel->n;
	theModel->copy /= theModel->n;
	theModel->copz /= theModel->n;
	for (i = 0; i<theModel->n; i++) {
		for (j = 0; j<i; j++) {
			dx = theModel->x[i] - theModel->x[j];
			dy = theModel->y[i] - theModel->y[j];
			dz = theModel->z[i] - theModel->z[j];
			r2 = dx * dx + dy * dy + dz * dz;
			if (r2>theModel->srad2[j] && r2>theModel->srad2[i]) {
				r = sqrt(r2 + theModel->softening_factor*
					theModel->softening_factor);
				theModel->PE -= theModel->G*
					theModel->mass[i] * theModel->mass[j] * r2 / (r*r*r);
			}
		}
	}
}

extern "C" __declspec(dllexport) void randomizeMassesNbodyModel(NbodyModel * theModel) {
	int i;
	for (i = 0; i<theModel->n; i++) {
		//theModel->mass[i]=fabs(drand_norm(theModel->default_mass,theModel->default_mass/5,0.0));
		theModel->mass[i] = drand(0.0, theModel->default_mass*2.0);
	}
}

extern "C" __declspec(dllexport) double computeSoftenedRadius(double g_m, double tstep_squared, double srad_factor) {
	// g_m = G*mass;
	if (srad_factor>0.0) {
		return srad_factor * fcr(g_m*tstep_squared, 0);
	}
	else {
		return 0.0;
	}
}
