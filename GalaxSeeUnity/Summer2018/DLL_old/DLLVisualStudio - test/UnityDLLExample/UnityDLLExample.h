#pragma once

// UnityDLLExample.h : Defines the exported functions for the DLL application.
//

#ifndef NBODY
#define NBODY
#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pppm_structs.h"
#include "octtree_structs.h"
#include "readline.h"

#define PI 3.14159

#define UPDATEMETHOD_HASH_TEXT 1
#define UPDATEMETHOD_BRIEF_TEXT 2
#define UPDATEMETHOD_VERBOSE_POSITIONS 4
#define UPDATEMETHOD_GD_IMAGE 8
#define UPDATEMETHOD_TEXT11 16
#define UPDATEMETHOD_X11 32
#define UPDATEMETHOD_VERBOSE_STATISTICS 64
#define UPDATEMETHOD_SDL 128
#define UPDATEMETHOD_DUMP 256

#define INT_METHOD_RK4          1
#define INT_METHOD_LEAPFROG     2
#define INT_METHOD_MPEULER      3
#define INT_METHOD_IEULER       4
#define INT_METHOD_EULER        5
#define INT_METHOD_ABM          6

#define FORCE_METHOD_DIRECT	1
#define FORCE_METHOD_TREE	2
#define FORCE_METHOD_PPPM	3

#define DISTRIBUTION_SPHERICAL_RANDOM 1
#define DISTRIBUTION_RECTANGULAR_RANDOM 2
#define DISTRIBUTION_RECTANGULAR_UNIFORM 3

typedef struct {
	int depth;
	double min_x;
	double max_x;
	double min_y;
	double max_y;
	double min_z;
	double max_z;
	double range2;
	void ** nodes;
	void * parent;
	double com_x;
	double com_y;
	double com_z;
	double mass;
	double srad2;
	int * stars_contained;
	int n_stars_contained;
	int order[8];
} OctTree;

typedef struct {
	double x;
	double y;
	double z;
}point3d;

// model structure
typedef struct {
	int n;			// number of points
	double * mass;		// masses
	double * x;                 // x values
	double * y;			// y values
	double * z;			// z values
	double * vx;		// x velocities
	double * vy;		// y velocities
	double * vz;		// z velocities
	int abmCounter;		// restart counter for ABM integration
	int bhCounter;		// counter for BarnesHut method to determine
						// when to recreate tree
	double default_mass;	// default star mass used for initialization
	double default_scale;	// (1/2) the "box" side length, typical scale
	double default_G;		// Gravitational constant in given units
	double KE;			// Kinetic energy of system
	double PE;			// Potential energy of system
	double comx;		// x center of mass
	double comy;		// y center of mass
	double comz;		// z center of mass
	double copx;		// x system momentum
	double copy;		// y system momentum
	double copz;		// z system momentum
	double rmsd;		// root mean square distance from origin
	double rmsp;		// root mean square momentum
	double * draw_x;		// copy of x for update purposes
	double * draw_y;		// copy of y for update purposes
	double * draw_z;		// copy of z for update purposes
	double * srad2;		// shield radius squared
	double * X;			// Computational scratch space
	double * X2;		// Computational scratch space
	double * XPRIME;		// Computational scratch space
	double * XPRIME1;		// Computational scratch space
	double * XPRIME2;		// Computational scratch space
	double * XPRIME3;		// Computational scratch space
	double * XPRIME4;		// Computational scratch space
	double * color3;		// Display color for SDL method
	double G;			// Gravitational constant
	double t;			// model time
	double tFinal;		// model end time
	double tstep;		// model time step
	double tstep_last;		// previously used time step
	double srad_factor;		// coefficient for automatic sheild radius
	double softening_factor;	// softening radius
	double rotation_factor;	// initial rotation, scaled so that 1
							// is "equilibrium" rotation
	double treeRangeCoefficient;// multiple of Barnes-Hut node size scale
								// that determine how far away an object
								// must be form the current node to use
								// a tree approximation
	double initial_v;		// initial random velocity
	OctTree * rootTree;		// Barnes-Hut tree structure
	OctTree ** treeList;	// map of each star to its place in the tree
	//PPPM * thePPPM;		// PPPM memory structure
	int iteration;		// current iteration
	int int_method;		// integration method
	int force_method;		// force method
	//char prefix[READLINE_MAX];  // prefix for output files
	double drag;		// drag coefficient
	double expansion;		// expansion rate (velocity per distance units)
	double pppm_ksigma;		// coefficient for determining PPPM softening
	double pppm_knear; 		// coefficient for determining PPPM nearest
							// neghbor cutoff
	double anisotropy;		// additional anisotropy added to uniform
							//  distributions
	int distribution;		// initial distribution
	time_t force_begin;
	time_t force_end;
	double force_total;
	double pointsize;
} NbodyModel;

extern "C" __declspec(dllexport) void setX(void * voo, double value, int i);
extern "C" __declspec(dllexport) void setXArray(void * voo, double * value);
extern "C" __declspec(dllexport) double getX(void * voo, int i);
extern "C" __declspec(dllexport) double * getXArray(void * voo);

extern "C" __declspec(dllexport) void allocateNbodyModel(int n, int ngrid);
extern "C" __declspec(dllexport) void initializeNbodyModel(NbodyModel *theModel);
extern "C" __declspec(dllexport) void freeNbodyModel(NbodyModel *theModel);
extern "C" __declspec(dllexport) void spinNbodyModel(NbodyModel * theModel);
extern "C" __declspec(dllexport) void speedNbodyModel(NbodyModel * theModel);
extern "C" __declspec(dllexport) void randomizeMassesNbodyModel(NbodyModel * theModel);
extern "C" __declspec(dllexport) void updateNbodyModel(NbodyModel *theModel, int updateMethod);
extern "C" __declspec(dllexport) double computeSoftenedRadius(double g_m, double tstep, double srad_factor);
extern "C" __declspec(dllexport) void printStatistics(NbodyModel *theModel);
extern "C" __declspec(dllexport) void calcStatistics(NbodyModel *theModel);
void nbodyEvents();
extern "C" __declspec(dllexport) void setPointsizeNbodyModel(NbodyModel *theModel, double pointsize);
extern "C" __declspec(dllexport) void setAnisoptropyNbodyModel(NbodyModel *theModel, double anisotropy);
extern "C" __declspec(dllexport) void setDistributionNbodyModel(NbodyModel *theModel, int distribution);
extern "C" __declspec(dllexport) void setPPPMCoeffsNbodyModel(NbodyModel *theModel, double ksigma, double knear);
extern "C" __declspec(dllexport) void setDragNbodyModel(NbodyModel *theModel, double drag);
extern "C" __declspec(dllexport) void setExpansionNbodyModel(NbodyModel *theModel, double expansion);
extern "C" __declspec(dllexport) void setPrefixNbodyModel(NbodyModel *theModel, const char * prefix);
extern "C" __declspec(dllexport) void setDefaultsNbodyModel(NbodyModel *theModel);
extern "C" __declspec(dllexport) void setMassNbodyModel(NbodyModel *theModel, double mass);
extern "C" __declspec(dllexport) void setScaleNbodyModel(NbodyModel *theModel, double scale);
extern "C" __declspec(dllexport) void setGNbodyModel(NbodyModel *theModel, double G);
extern "C" __declspec(dllexport) void setTFinal(NbodyModel *theModel, double tFinal);
extern "C" __declspec(dllexport) void setTStep(NbodyModel *theModel, double tStep);
extern "C" __declspec(dllexport) void setIntMethod(NbodyModel *theModel, int int_method);
extern "C" __declspec(dllexport) void setForceMethod(NbodyModel *theModel, int force_method);
extern "C" __declspec(dllexport) void setTreeRangeCoefficient(NbodyModel *theModel, double coefficient);
extern "C" __declspec(dllexport) void setSofteningNbodyModel(NbodyModel *theModel, double softening_factor);
extern "C" __declspec(dllexport) void setSradNbodyModel(NbodyModel *theModel, double srad_factor);
extern "C" __declspec(dllexport) void setRotationFactor(NbodyModel *theModel, double rotation_factor);
extern "C" __declspec(dllexport) void setInitialV(NbodyModel *theModel, double initial_v);
extern "C" __declspec(dllexport) void calcDerivs(double * x, double * derivs, double t, double tStep,
	NbodyModel * theModel);
extern "C" __declspec(dllexport) void calcDerivsDirect(double * x, double * derivs, double t, double tStep,
	NbodyModel * theModel);
extern "C" __declspec(dllexport) void calcDerivsBarnesHut(double * x, double * derivs, double t, double tStep,
	NbodyModel * theModel);
extern "C" __declspec(dllexport) void calcDerivsPPPM(double * x, double * derivs, double t, double tStep,
	NbodyModel * theModel);
extern "C" __declspec(dllexport) void stepNbodyModelEuler(NbodyModel * theModel, double tStep);
extern "C" __declspec(dllexport) void stepNbodyModelIEuler(NbodyModel * theModel, double tStep);
extern "C" __declspec(dllexport) void stepNbodyModelMPEuler(NbodyModel * theModel, double tStep);
extern "C" __declspec(dllexport) void stepNbodyModelRK4(NbodyModel * theModel, double tStep);
extern "C" __declspec(dllexport) void stepNbodyModelLeapfrog(NbodyModel * theModel, double tStep);
extern "C" __declspec(dllexport) void stepNbodyModelABM(NbodyModel * theModel, double tStep);
extern "C" __declspec(dllexport) void stepNbodyModel(NbodyModel * theModel);
extern "C" __declspec(dllexport) void copy2X(NbodyModel *theModel);
extern "C" __declspec(dllexport) void copy2xyz(NbodyModel *theModel);

#endif
