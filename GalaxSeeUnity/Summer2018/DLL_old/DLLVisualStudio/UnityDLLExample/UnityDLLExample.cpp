// UnityDLLExample.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <math.h>
#include "UnityDLLExample.h"

extern "C" __declspec(dllexport)  double randRange(double low, double high) {
	double r = (double)rand() / (double)RAND_MAX;
	return low + r*(high - low);
}

//softening potentials
extern "C" __declspec(dllexport) void setSofteningNBodyModel(void * voo, double softening_factor) {
	Model * foo = (Model *)voo;
	foo->softening_factor = softening_factor;
}

//shield radius methods
extern "C" __declspec(dllexport) void setSradNbodyModel(void * voo, double srad_factor)
{
	Model *foo = (Model *)voo;
	foo->srad_factor = srad_factor;
}

extern "C" __declspec(dllexport) double computeSoftenedRadius(double g_m, double tstep_squared, double srad_factor) {
	 //g_m = G*m;
	if (srad_factor>0.0) {
		return srad_factor * fcr(g_m*tstep_squared, 0);
	}
	else {
		return 0.0;
	}
}


/*Shield radius methods... might make into seperate class*/
extern "C" _declspec(dllexport) double fcr(double x, int MAX_ITER) {
	double a, b;
	int done = false;
	int count = 0;
	int sign = 0;

	if (x == 0.0) return 0.0;
	if (x<0.0) { x = -x; sign = 1; }
	if (x == 1.0) { done = true; }
	a = fcr_guess(x);
	b = a;

	while (!done&&count++<MAX_ITER) {
		b = a * (((a*a*a + x) + x) / (a*a*a + (a*a*a + x)));
		if (fabs((b - a) / a)<1.0e-6) done = true;
		else if (fabs(b - a)<1.0e-50) done = true;
		a = b;
	}
	if (sign) return -b;
	else return b;
}

extern "C" _declspec(dllexport) double fcr_guess(double x) {
	if (x > 1.0) {
		if (x < 1.0e1) return 2.0e0;
		else if (x < 1.0e2) return 5.0e0;
		else if (x < 1.0e3) return 1.0e1;
		else if (x < 1.0e4) return 2.0e1;
		else if (x < 1.0e5) return 5.0e1;
		else if (x < 1.0e6) return 1.0e2;
		else if (x < 1.0e7) return 2.0e2;
		else if (x < 1.0e8) return 5.0e2;
		else if (x < 1.0e9) return 1.0e3;
		else if (x < 1.0e10) return 2.0e3;
		else if (x < 1.0e11) return 5.0e3;
		else if (x < 1.0e12) return 1.0e4;
		else if (x < 1.0e15) return 1.0e5;
		else if (x < 1.0e18) return 1.0e6;
		else if (x < 1.0e21) return 1.0e7;
		else if (x < 1.0e24) return 1.0e8;
		else if (x < 1.0e27) return 1.0e9;
		else return 1.0e10;
	}
	else if (x < 1.0) {
		if (x > 1.0e-1) return 5.0e-1;
		else if (x > 1.0e-2) return 2.0e-1;
		else if (x > 1.0e-3) return 1.0e-1;
		else if (x > 1.0e-4) return 5.0e-2;
		else if (x > 1.0e-5) return 2.0e-2;
		else if (x > 1.0e-6) return 1.0e-2;
		else if (x > 1.0e-7) return 5.0e-3;
		else if (x > 1.0e-8) return 2.0e-3;
		else if (x > 1.0e-9) return 1.0e-3;
		else if (x > 1.0e-10) return 5.0e-4;
		else if (x > 1.0e-11) return 2.0e-4;
		else if (x > 1.0e-12) return 1.0e-4;
		else if (x > 1.0e-15) return 1.0e-5;
		else if (x > 1.0e-18) return 1.0e-6;
		else if (x > 1.0e-21) return 1.0e-7;
		else if (x > 1.0e-24) return 1.0e-8;
		else if (x > 1.0e-27) return 1.0e-9;
		else return 1.0e-10;
	}
	else if (x == 1.0) {
		return 1.0;
	}
	else {
		return 0.0;
	}
}

extern "C" __declspec(dllexport) void setMass(void * voo, double m) {
	Model * foo = (Model *)voo;
	foo->m = m;
}

extern "C" __declspec(dllexport) void setMasses(void * voo, double * m) {
	Model * foo = (Model *)voo;
	int n = foo->n;
	foo->m = 0.0;
	for (int i = 0; i < n; i++) {
		foo->mass[i] = m[i];
		foo->m += m[i];
	}
}

extern "C" __declspec(dllexport) void setG(void * voo, double G) {
	Model * foo = (Model *)voo;
	foo->G = G;
}

/*extern "C" _declspec(dllexport) void setTStep(void * voo, double tstep) {
	Model * foo = (Model *)voo;
	foo->t = tstep;
}*/

extern "C" __declspec(dllexport) void * allocModel(int n) {
	Model *foo = (Model *)malloc(sizeof(Model));
	foo->n = n;
	foo->x = (double *)malloc(sizeof(double) * 6 * n);
	foo->x2 = (double *)malloc(sizeof(double) * 6 * n);
	foo->store = (double *)malloc(sizeof(double) * 6 * n);
	foo->storep = (double *)malloc(sizeof(double) * 6 * n);
	foo->k1 = (double *)malloc(sizeof(double) * 6 * n);
	foo->k2 = (double *)malloc(sizeof(double) * 6 * n);
	foo->k3 = (double *)malloc(sizeof(double) * 6 * n);
	foo->k4 = (double *)malloc(sizeof(double) * 6 * n);
	foo->XPRIME = (double *)malloc(sizeof(double)*n * 6);
	foo->XPRIME1 = (double *)malloc(sizeof(double)*n * 6);
	foo->XPRIME2 = (double *)malloc(sizeof(double)*n * 6);
	foo->XPRIME3 = (double *)malloc(sizeof(double)*n * 6);
	foo->XPRIME4 = (double *)malloc(sizeof(double)*n * 6);
	foo->srad2 = (double *)malloc(sizeof(double)*n * 6); //shield radius array
	foo->mass = (double *)malloc(sizeof(double)*n);
	foo->abmCounter = -3; 
	foo->LeapFrogFirstIteration = true;
	foo->shield = 1.0e-4;
	foo->softening_factor = 1.0e-4;
	foo->G = 1.0;
	foo->m = 1.0;

	return foo;
}

extern "C" __declspec(dllexport) void initModel(void * voo) {
	Model *foo = (Model *)voo;
	int n = foo->n;


	for (int i = 0; i < n; i++) {
		foo->x[i * 6 + 0] = randRange(-1, 1);
		foo->x[i * 6 + 1] = randRange(-1, 1);
		foo->x[i * 6 + 2] = randRange(-1, 1);
		foo->x[i * 6 + 3] = 0.0;
		foo->x[i * 6 + 4] = 0.0;
		foo->x[i * 6 + 5] = 0.0;
		foo->mass[i] = foo->m / (double)n;
	}

}


extern "C" __declspec(dllexport) void stepModelEuler(void * voo, double h) {
	Model * foo = (Model *)voo;

	int nEquations = foo->n*6;

	ratesOfChange(foo, foo->x, foo->k1);
	for (int i = 0; i < nEquations; i++) {
		foo->x[i] = foo->x[i] + foo->k1[i] * h;
	}
}


extern "C" __declspec(dllexport) void stepModelRK4(void * voo, double h) {
	Model * foo = (Model *)voo;

	int nEquations = foo->n*6;

	ratesOfChange(foo,foo->x, foo->k1);
	for (int i = 0; i < nEquations; i++) {
		foo->store[i] = foo->x[i] + foo->k1[i] * h / 2.0;
	}
	ratesOfChange(foo,foo->store, foo->k2);
	for (int i = 0; i < nEquations; i++) {
		foo->store[i] = foo->x[i] + foo->k2[i] * h / 2.0;
	}
	ratesOfChange(foo,foo->store, foo->k3);
	for (int i = 0; i < nEquations; i++) {
		foo->store[i] = foo->x[i] + foo->k3[i] * h;
	}
	ratesOfChange(foo,foo->store, foo->k4);
	for (int i = 0; i < nEquations; i++) {
		foo->x[i] = foo->x[i] + (foo->k1[i] + 2.0*foo->k2[i] + 2.0*foo->k3[i] + foo->k4[i]) * h / 6.0;
		//storep is a more accurate guess of actual value 
		//use in predictor corrector for best guess
		foo->storep[i] = (foo->k1[i] + 2.0*foo->k2[i] + 2.0*foo->k3[i] + foo->k4[i]) / 6.0;
	}
}

//Leap frog method 
extern "C" __declspec(dllexport) void stepNbodyModelLeapfrog(void * voo, double tStep) 
{
	Model * foo = (Model *)voo;

	ratesOfChange(foo, foo->x, foo->k1);
	if (foo->LeapFrogFirstIteration) {
		// setup leapfrog on first step, change velocities by a half step
		for (int i = 0; i < foo->n; i++) {
			foo->x[i * 6 + 3] += 0.5*foo->k1[i * 6 + 3] * tStep;
			foo->x[i * 6 + 4] += 0.5*foo->k1[i * 6 + 4] * tStep;
			foo->x[i * 6 + 5] += 0.5*foo->k1[i * 6 + 5] * tStep;
		}
	}
	else {
		// update v,x
		for (int i = 0; i < foo->n * 6; i++) {
			foo->x[i] += foo->k1[i] * tStep;
		}
	}
	foo->LeapFrogFirstIteration=false;
}

/*Predictor Corrector*/
extern "C" __declspec(dllexport) void stepNbodyModelABM(void * voo, double tStep) {
	Model *foo = (Model*)voo;

	double * fk3 = NULL;
	double * fk2 = NULL;
	double * fk1 = NULL;
	double * fk0 = NULL;
	double * fkp = NULL;

	// determine if previous steps exist, if not, populate w/ RK4
	if (foo->abmCounter < 0) {  //counter starts at -3 because ABM needs certain information before it can run
		stepModelRK4(foo, tStep);  //RK4 step first 
		if (foo->abmCounter == -3) {
			for (int i = 0; i < foo->n * 6; i++)
				foo->XPRIME4[i] = foo->storep[i];
		}
		else if (foo->abmCounter == -2) {
			for (int i = 0; i < foo->n * 6; i++)
				foo->XPRIME3[i] = foo->storep[i];
		}
		else {
			for (int i = 0; i < foo->n * 6; i++)
				foo->XPRIME2[i] = foo->storep[i];
		}
	}
	else {
		if (foo->abmCounter % 5 == 0) {
			fk3 = foo->XPRIME4;
			fk2 = foo->XPRIME3;
			fk1 = foo->XPRIME2;
			fk0 = foo->XPRIME1;
			fkp = foo->XPRIME;
		}
		else if (foo->abmCounter % 5 == 1) {
			fk3 = foo->XPRIME3;
			fk2 = foo->XPRIME2;
			fk1 = foo->XPRIME1;
			fk0 = foo->XPRIME;
			fkp = foo->XPRIME4;
		}
		else if (foo->abmCounter % 5 == 2) {
			fk3 = foo->XPRIME2;
			fk2 = foo->XPRIME1;
			fk1 = foo->XPRIME;
			fk0 = foo->XPRIME4;
			fkp = foo->XPRIME3;
		}
		else if (foo->abmCounter % 5 == 3) {
			fk3 = foo->XPRIME1;
			fk2 = foo->XPRIME;
			fk1 = foo->XPRIME4;
			fk0 = foo->XPRIME3;
			fkp = foo->XPRIME2;
		}
		else if (foo->abmCounter % 5 == 4) {
			fk3 = foo->XPRIME;
			fk2 = foo->XPRIME4;
			fk1 = foo->XPRIME3;
			fk0 = foo->XPRIME2;
			fkp = foo->XPRIME1;
		}
		ratesOfChange(foo, foo->x,fk0);
		for (int i = 0; i < foo->n * 6; i++) { //predictor step
			foo->x2[i] = foo->x[i] +
				(tStep / 24.0)*(-9.0*fk3[i] + 37.0*fk2[i]
					- 59.0*fk1[i] + 55.0*fk0[i]);
		}
		ratesOfChange(foo, foo->x2, fkp);
		for (int i = 0; i < foo->n * 6; i++) { //correction step
			foo->x[i] = foo->x[i] +
				(tStep / 24.0)*(fk2[i] - 5.0*fk1[i] +
					19.0*fk0[i] + 9.0*fkp[i]);
		}
	}
	foo->abmCounter++;
}

extern "C" __declspec(dllexport) void stepModelRK2(void * voo, double dt) {
	Model * foo = (Model *)voo;
	for (int i = 0; i < 6*foo->n; i++) {
		foo->store[i] = foo->x[i];
	}
	ratesOfChange(foo, foo->x, foo->k1);
	for (int i = 0; i < 6 * foo->n; i++) {
		foo->x[i] += foo->k1[i] * dt;
	}
	ratesOfChange(foo, foo->x, foo->k2);
	for (int i = 0; i < 6 * foo->n; i++) {
		foo->x[i] = foo->store[i]+0.5*(foo->k1[i]+foo->k2[i]) * dt;
	}
}

extern "C" __declspec(dllexport) void setX(void * voo,double value, int i) {
	Model * foo = (Model *)voo;
	foo->x[i] = value;
}

extern "C" __declspec(dllexport) void setXArray(void * voo, double * value) {
	Model * foo = (Model *)voo;
	int n = foo->n;
	for (int i = 0; i < 6*n; i++) {
		foo->x[i] = value[i];
	}
}

extern "C" __declspec(dllexport) double getX(void * voo, int i) {
	Model * foo = (Model *)voo;
	return foo->x[i];
}

extern "C" __declspec(dllexport) double * getXArray(void * voo) {
	Model * foo = (Model *)voo;
	return foo->x;
}


extern "C" __declspec(dllexport) void destroyModel(void * voo) {
	Model * foo = (Model *)voo;
	free(foo->x);
	free(foo->x2);
	free(foo->mass);
	free(foo->store);
	free(foo->storep);
	free(foo->XPRIME);
	free(foo->XPRIME1);
	free(foo->XPRIME2);
	free(foo->XPRIME3);
	free(foo->XPRIME4);
	free(foo->k1);
	free(foo->k2);
	free(foo->k3);
	free(foo->k4);
	free(foo->srad2);
	// add all the other frees
	free(foo);
}

void ratesOfChangeBarnesHut(double * x, double * derivs, double t, double tStep,
	Model * foo) {
	int i;
	point3d accel;
	for (i = 0; i<foo->n; i++) {
		if (foo->treeList[i] != NULL) {
			(foo->treeList[i])->com_x = x[i * 6];
			(foo->treeList[i])->com_y = x[i * 6 + 1];
			(foo->treeList[i])->com_z = x[i * 6 + 2];
		}
	}
	resetOctTree(foo->rootTree, foo);
	for (i = 0; i<foo->n; i++) {
		derivs[i * 6 + 0] = x[i * 6 + 3];
		derivs[i * 6 + 1] = x[i * 6 + 4];
		derivs[i * 6 + 2] = x[i * 6 + 5];
		accel = calculateForceOctTree(1, i, x[i * 6], x[i * 6 + 1], x[i * 6 + 2],
			foo->G, foo->rootTree, foo->softening_factor);
		derivs[i * 6 + 3] = accel.x - foo->drag*x[i * 6 + 3] / foo->mass[i];
		derivs[i * 6 + 4] = accel.y - foo->drag*x[i * 6 + 4] / foo->mass[i];
		derivs[i * 6 + 5] = accel.z - foo->drag*x[i * 6 + 5] / foo->mass[i];
	}
}

OctTree * allocOctTree() {
	OctTree * retval = malloc(sizeof(OctTree));
	initOctTree(retval);
	return retval;
}

void initOctTree(OctTree * theTree) {
	theTree->stars_contained = NULL;
	theTree->nodes = NULL;
}

void populateOctTree(OctTree * theTree, OctTree ** theList,
	Model * foo,
	int depth, int * stars_contained, int n_stars_contained,
	double min_x, double max_x,
	double min_y, double max_y, double min_z, double max_z) {
	int i, q, count;
	double q_min_x;
	double q_max_x;
	double ave_x;
	double q_min_y;
	double q_max_y;
	double ave_y;
	double q_min_z;
	double q_max_z;
	double ave_z;
	int * work;
	OctTree * node;
	theTree->min_x = min_x;
	theTree->max_x = max_x;
	theTree->min_y = min_y;
	theTree->max_y = max_y;
	theTree->min_z = min_z;
	theTree->max_z = max_z;
	theTree->range2 = foo->treeRangeCoefficient*
		max(max_x - min_x, max(max_y - min_y, max_z - min_z));
	theTree->range2 *= theTree->range2;
	theTree->depth = depth;
	theTree->n_stars_contained = n_stars_contained;
	if (theTree->stars_contained != NULL) free(theTree->stars_contained);
	theTree->stars_contained = alloc_iarray(n_stars_contained);
	work = alloc_iarray(n_stars_contained);
	theTree->com_x = 0.0;
	theTree->com_y = 0.0;
	theTree->com_z = 0.0;
	theTree->mass = 0.0;
	for (i = 0; i<n_stars_contained; i++) {
		if (stars_contained != NULL) {
			theTree->stars_contained[i] = stars_contained[i];
		}
		else {
			theTree->stars_contained[i] = i;
		}
		theTree->com_x += foo->mass[theTree->stars_contained[i]] *
			foo->x[theTree->stars_contained[i]];
		theTree->com_y += theModel->mass[theTree->stars_contained[i]] *
			theModel->y[theTree->stars_contained[i]];
		theTree->com_z += theModel->mass[theTree->stars_contained[i]] *
			theModel->z[theTree->stars_contained[i]];
		theTree->mass += theModel->mass[theTree->stars_contained[i]];
	}
	theTree->com_x /= theTree->mass;
	theTree->com_y /= theTree->mass;
	theTree->com_z /= theTree->mass;
	theTree->srad2 = computeSoftenedRadius(
		theModel->G*theTree->mass, theModel->tstep*theModel->tstep,
		theModel->srad_factor);
	theTree->srad2 *= theTree->srad2;
	if (n_stars_contained == 1) {
		theTree->com_x = theModel->x[theTree->stars_contained[0]];
		theTree->com_y = theModel->y[theTree->stars_contained[0]];
		theTree->com_z = theModel->z[theTree->stars_contained[0]];
		theTree->mass = theModel->mass[theTree->stars_contained[0]];
		if (theList != NULL)
			theList[theTree->stars_contained[0]] = theTree;
		if (theTree->nodes != NULL) free(theTree->nodes);
		theTree->nodes = NULL;
	}
	else if (n_stars_contained>1) {
		if (theTree->nodes == NULL)
			theTree->nodes = (void **)malloc(sizeof(OctTree *) * 8);
		ave_x = (min_x + max_x) / 2.0;
		ave_y = (min_y + max_y) / 2.0;
		ave_z = (min_z + max_z) / 2.0;
		for (q = 0; q<8; q++) {
			switch (q) {
			case 0:
				q_min_x = ave_x;
				q_min_y = ave_y;
				q_min_z = ave_z;
				q_max_x = max_x;
				q_max_y = max_y;
				q_max_z = max_z;
				break;
			case 1:
				q_min_x = min_x;
				q_min_y = ave_y;
				q_min_z = ave_z;
				q_max_x = ave_x;
				q_max_y = max_y;
				q_max_z = max_z;
				break;
			case 2:
				q_min_x = min_x;
				q_min_y = min_y;
				q_min_z = ave_z;
				q_max_x = ave_x;
				q_max_y = ave_y;
				q_max_z = max_z;
				break;
			case 3:
				q_min_x = ave_x;
				q_min_y = min_y;
				q_min_z = ave_z;
				q_max_x = max_x;
				q_max_y = ave_y;
				q_max_z = max_z;
				break;
			case 4:
				q_min_x = ave_x;
				q_min_y = ave_y;
				q_min_z = min_z;
				q_max_x = max_x;
				q_max_y = max_y;
				q_max_z = ave_z;
				break;
			case 5:
				q_min_x = min_x;
				q_min_y = ave_y;
				q_min_z = min_z;
				q_max_x = ave_x;
				q_max_y = max_y;
				q_max_z = ave_z;
				break;
			case 6:
				q_min_x = min_x;
				q_min_y = min_y;
				q_min_z = min_z;
				q_max_x = ave_x;
				q_max_y = ave_y;
				q_max_z = ave_z;
				break;
			case 7:
				q_min_x = ave_x;
				q_min_y = min_y;
				q_min_z = min_z;
				q_max_x = max_x;
				q_max_y = ave_y;
				q_max_z = ave_z;
				break;
			}
			count = 0;
			for (i = 0; i<n_stars_contained; i++) {
				if (theModel->x[theTree->stars_contained[i]] >= q_min_x &&
					theModel->x[theTree->stars_contained[i]]<q_max_x &&
					theModel->y[theTree->stars_contained[i]] >= q_min_y &&
					theModel->y[theTree->stars_contained[i]]<q_max_y &&
					theModel->z[theTree->stars_contained[i]] >= q_min_z &&
					theModel->z[theTree->stars_contained[i]]<q_max_z)
					work[count++] = theTree->stars_contained[i];
			}
			if (count == 0) {
				theTree->nodes[q] = NULL;
			}
			else {
				node = allocOctTree();
				node->parent = theTree;
				populateOctTree(node, theList, theModel,
					depth + 1, work, count,
					q_min_x, q_max_x,
					q_min_y, q_max_y, q_min_z, q_max_z);
				theTree->nodes[q] = node;
			}
		}
	}
	free(work);
	return;
}

void resetOctTree(OctTree * theTree, NbodyModel * theModel) {
	double sum_x, sum_y, sum_z;
	int q, i;
	if (theTree->nodes != NULL) {
		sum_x = 0.0;
		sum_y = 0.0;
		sum_z = 0.0;
		for (q = 0; q<8; q++) {
			if (theTree->nodes[q] != NULL) {
				resetOctTree(theTree->nodes[q], theModel);
				sum_x += ((OctTree *)theTree->nodes[q])->com_x*
					((OctTree *)theTree->nodes[q])->mass;
				sum_y += ((OctTree *)theTree->nodes[q])->com_y*
					((OctTree *)theTree->nodes[q])->mass;
				sum_z += ((OctTree *)theTree->nodes[q])->com_z*
					((OctTree *)theTree->nodes[q])->mass;
			}
		}
		sum_x /= theTree->mass;
		sum_y /= theTree->mass;
		sum_z /= theTree->mass;
		theTree->com_x = sum_x;
		theTree->com_y = sum_y;
		theTree->com_z = sum_z;
		sum_x = 0.0;
		sum_y = 0.0;
		sum_z = 0.0;
		for (i = 0; i<theTree->n_stars_contained; i++) {
			sum_x += theModel->mass[theTree->stars_contained[i]] *
				theModel->x[theTree->stars_contained[i]];
			sum_y += theModel->mass[theTree->stars_contained[i]] *
				theModel->y[theTree->stars_contained[i]];
			sum_z += theModel->mass[theTree->stars_contained[i]] *
				theModel->z[theTree->stars_contained[i]];
		}
		theTree->com_x = sum_x / theTree->mass;
		theTree->com_y = sum_y / theTree->mass;
		theTree->com_z = sum_z / theTree->mass;
	}
}


point3d calculateForceOctTree(int minDepth,
	int index, double xin, double yin, double zin,
	double G, OctTree * theTree, double soft) {
	point3d accel;
	point3d q_accel;
	double dx, dy, dz, r, r2, r3, r3i;
	int q, i_order;


	accel.x = 0.0;
	accel.y = 0.0;
	accel.z = 0.0;
	if (theTree->n_stars_contained == 1) {
		if (theTree->stars_contained[0] == index) {
			return accel;
		}
		else {
			// return values based on COM
			dx = xin - theTree->com_x;
			dy = yin - theTree->com_y;
			dz = zin - theTree->com_z;
			r2 = dx * dx + dy * dy + dz * dz + soft * soft;
			if (r2<theTree->srad2) {
				return accel;
			}
			else {
				r = sqrt(r2);
				r3i = G / (r*r2);
				accel.x = -theTree->mass*dx*r3i;
				accel.y = -theTree->mass*dy*r3i;
				accel.z = -theTree->mass*dz*r3i;
				return accel;
			}
		}
	}
	else {
		// if depth too low or distance within NRAD
		dx = xin - theTree->com_x;
		dy = yin - theTree->com_y;
		dz = zin - theTree->com_z;
		r2 = dx * dx + dy * dy + dz * dz + soft * soft;
		// order of descent -> not near and not in first
		// near but not in second
		// in third

		if (theTree->depth<minDepth || r2<theTree->range2 ||
			(xin<theTree->max_x&&xin >= theTree->min_x&&
				yin<theTree->max_y&&yin >= theTree->min_y&&
				zin<theTree->max_z&&zin >= theTree->min_z)
			) {
			for (q = 0; q<8; q++) {
				if (theTree->nodes[q] != NULL) {
					if (r2>((OctTree*)theTree->nodes[q])->range2) {
						theTree->order[q] = 1;
					}
					else {
						theTree->order[q] = 2;
					}
				}
			}
			for (i_order = 1; i_order <= 2; i_order++) {
				for (q = 0; q<8; q++) {
					if (theTree->nodes[q] != NULL && theTree->order[q] == i_order) {
						q_accel = calculateForceOctTree(minDepth,
							index, xin, yin, zin,
							i                            G, theTree->nodes[q], soft);
						accel.x += q_accel.x;
						accel.y += q_accel.y;
						accel.z += q_accel.z;
					}
				}
			}
			return accel;
		}
		else {
			//return value based on COM
			if (r2<theTree->srad2) {
				return accel;
			}
			else {
				r = sqrt(r2);
				r3 = r * r2;
				accel.x = -theTree->mass*G*dx / r3;
				accel.y = -theTree->mass*G*dy / r3;
				accel.z = -theTree->mass*G*dz / r3;
				return accel;
			}
		}
	}
}

void freeOctTree(OctTree * theTree, int depth) {
	int q;
	if (theTree == NULL) return;
	if (theTree->nodes != NULL) {
		for (q = 0; q<8; q++) {
			if (theTree->nodes[q] != NULL) {
				freeOctTree(theTree->nodes[q], depth);
			}
		}
	}
	if (theTree->depth >= depth) {
		if (theTree->nodes != NULL)free(theTree->nodes);
		if (theTree->stars_contained != NULL)free(theTree->stars_contained);
		free(theTree);
	}
}



extern "C" __declspec(dllexport) void ratesOfChange(Model * foo, double* x, double* xdot) {
	int n = foo->n;
	double srad, tstep_squared;
	for (int i = 0; i < n; i++) {
		xdot[i * 6 + 0] = x[i * 6 + 3];
		xdot[i * 6 + 1] = x[i * 6 + 4];
		xdot[i * 6 + 2] = x[i * 6 + 5];
		xdot[i * 6 + 3] = 0.0;
		xdot[i * 6 + 4] = 0.0;
		xdot[i * 6 + 5] = 0.0;
		srad = computeSoftenedRadius(foo->G*foo->mass[i], tstep_squared, foo->srad_factor);
		foo->srad2[i] = srad * srad;
	}
	for (int i = 0; i < n; i++) {
		double xi = x[i * 6 + 0];
		double yi = x[i * 6 + 1];
		double zi = x[i * 6 + 2];
		double axi = 0.0;
		double ayi = 0.0;
		double azi = 0.0;
		{
			for (int j = i + 1; j < n; j++) {
				double xj = x[j * 6 + 0];
				double yj = x[j * 6 + 1];
				double zj = x[j * 6 + 2];
				double dx = xi - xj;
				double dy = yi - yj;
				double dz = zi - zj;

				double dr = sqrt(dx*dx + dy*dy + dz*dz + foo->softening_factor*foo->softening_factor);
				double dr2 = dr * dr;
				double dr3 = dr2*dr;
				double accel = -foo->G / dr3;
				if (dr2 > foo->srad2[j])
				{
					axi += accel * dx * foo->mass[j];
					ayi += accel * dy * foo->mass[j];
					azi += accel * dz * foo->mass[j];
				}
				if (dr2 > foo->srad2[i])
				{
					xdot[j * 6 + 3] -= accel * dx * foo->mass[i];
					xdot[j * 6 + 4] -= accel * dy * foo->mass[i];
					xdot[j * 6 + 5] -= accel * dz * foo->mass[i];
				}
			}
				/*
				double accel = -foo->G / dr2;
				axi += accel*dx / dr*foo->mass[j];
				ayi += accel*dy / dr*foo->mass[j];
				azi += accel*dz / dr*foo->mass[j];
				xdot[j * 6 + 3] -= accel*dx / dr*foo->mass[i];
				xdot[j * 6 + 4] -= accel*dy / dr*foo->mass[i];
				xdot[j * 6 + 5] -= accel*dz / dr*foo->mass[i];
			}
			*/
			{
				xdot[i * 6 + 3] += axi;
				xdot[i * 6 + 4] += ayi;
				xdot[i * 6 + 5] += azi;
			}
		}
	}
}

