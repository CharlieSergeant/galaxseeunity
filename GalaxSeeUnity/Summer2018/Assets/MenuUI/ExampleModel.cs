﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleModel : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void onMenuTrigger(int val) {
		string message = string.Format ("Model triggered by menu message {0}",val);
		Debug.Log (message);
	}
}
