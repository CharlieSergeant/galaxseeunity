﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MyDebug : MonoBehaviour {

    StreamWriter file;

	// Use this for initialization
	void Start () {
        file = new StreamWriter(@"c:\Users\CDS\Desktop\logfile.txt");
        file.AutoFlush = true;
	}
	
	public void Log(string line)
    {
        file.WriteLine(line);
        Debug.Log(line);
    }
}
