#pragma once

// UnityDLLExample.h : Defines the exported functions for the DLL application.
//

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include "pppm_structs.h"
#include "octtree_structs.h"

#define PI 3.14159


#define INT_METHOD_RK4          1
#define INT_METHOD_LEAPFROG     2
#define INT_METHOD_MPEULER      3
#define INT_METHOD_IEULER       4
#define INT_METHOD_EULER        5
#define INT_METHOD_ABM          6

#define FORCE_METHOD_DIRECT	1
#define FORCE_METHOD_TREE	2
#define FORCE_METHOD_PPPM	3

#define DISTRIBUTION_SPHERICAL_RANDOM 1
#define DISTRIBUTION_RECTANGULAR_RANDOM 2
#define DISTRIBUTION_RECTANGULAR_UNIFORM 3


// model structure
typedef struct {
	int n;			// number of points
	double * mass;		// masses
	double m; 
	double * x;                 // x values
	double * y;			// y values
	double * z;			// z values
	double * vx;		// x velocities
	double * vy;		// y velocities
	double * vz;		// z velocities
	int abmCounter;		// restart counter for ABM integration
	int bhCounter;		// counter for BarnesHut method to determine
						// when to recreate tree
	double default_mass;	// default star mass used for initialization
	double default_scale;	// (1/2) the "box" side length, typical scale
	double default_G;		// Gravitational constant in given units
	double KE;			// Kinetic energy of system
	double PE;			// Potential energy of system
	double comx;		// x center of mass
	double comy;		// y center of mass
	double comz;		// z center of mass
	double copx;		// x system momentum
	double copy;		// y system momentum
	double copz;		// z system momentum
	double rmsd;		// root mean square distance from origin
	double rmsp;		// root mean square momentum
	double * draw_x;		// copy of x for update purposes
	double * draw_y;		// copy of y for update purposes
	double * draw_z;		// copy of z for update purposes
	double * srad2;		// shield radius squared
	double * X;			// Computational scratch space
	double * X2;		// Computational scratch space
	double * XPRIME;		// Computational scratch space
	double * XPRIME1;		// Computational scratch space
	double * XPRIME2;		// Computational scratch space
	double * XPRIME3;		// Computational scratch space
	double * XPRIME4;		// Computational scratch space
	double * color3;		// Display color for SDL method
	double G;			// Gravitational constant
	double t;			// model time
	double tFinal;		// model end time
	double tstep;		// model time step
	double tstep_last;		// previously used time step
	double srad_factor;		// coefficient for automatic sheild radius
	double softening_factor;	// softening radius
	double rotation_factor;	// initial rotation, scaled so that 1
							// is "equilibrium" rotation
	double treeRangeCoefficient;// multiple of Barnes-Hut node size scale
								// that determine how far away an object
								// must be form the current node to use
								// a tree approximation
	double initial_v;		// initial random velocity
	OctTree * rootTree;		// Barnes-Hut tree structure
	OctTree ** treeList;	// map of each star to its place in the tree
	PPPM * thePPPM;			//PPPM * thePPPM;		// PPPM memory structure
	int iteration;		// current iteration
	int int_method;		// integration method
	int force_method;		// force method
	//char prefix[READLINE_MAX]; //char prefix[READLINE_MAX];  // prefix for output files
	double drag;		// drag coefficient
	double expansion;		// expansion rate (velocity per distance units)
	double pppm_ksigma;		// coefficient for determining PPPM softening
	double pppm_knear; 		// coefficient for determining PPPM nearest
							// neghbor cutoff
	double anisotropy;		// additional anisotropy added to uniform
							//  distributions
	int distribution;		// initial distribution
	time_t force_begin;
	time_t force_end;
	double force_total;
	double pointsize;
	FILE * logfile;
} NbodyModel;

extern "C" __declspec(dllexport) void * allocateNbodyModel(int n, int ngrid);
extern "C" __declspec(dllexport) void setPointsizeNbodyModel(void * voo, double pointsize);
extern "C" __declspec(dllexport) void setAnisotropyNbodyModel(void * voo, double anisotropy);
extern "C" __declspec(dllexport) void setDistributionNbodyModel(void * voo, int distribution);
extern "C" __declspec(dllexport) void setPPPMCoeffsNbodyModel(void * voo, double ksigma,double knear);
extern "C" __declspec(dllexport) void setExpansionNbodyModel(void * voo, double expansion);
extern "C" __declspec(dllexport) void setDragNbodyModel(void * voo, double pointsize);
//extern "C" __declspec(dllexport) void setPrefixNbodyModel(NbodyModel *theModel, char * prefix);
extern "C" __declspec(dllexport) void setSradNbodyModel(void * voo, double srad_factor);
extern "C" __declspec(dllexport) void setSofteningNbodyModel(void * voo, double pointsize);
extern "C" __declspec(dllexport) void setScaleNbodyModel(void* voo, double scale);
extern "C" __declspec(dllexport) void setMass(void * voo, double m);
extern "C" __declspec(dllexport) void setMasses(void * voo, double * m);
extern "C" __declspec(dllexport) void setG(void * voo, double G);
extern "C" __declspec(dllexport) void setRotationFactor(void * voo, double rotation_factor);
extern "C" __declspec(dllexport) void setInitialV(void * voo, double initial_v);
extern "C" __declspec(dllexport) void setTFinal(void * voo, double tFinal);
extern "C" __declspec(dllexport) void setInitialV(void * voo, double initial_v);
extern "C" __declspec(dllexport) void setModelT(void * voo, double time);
extern "C" __declspec(dllexport) void setIntMethod(void * voo, int int_method);
extern "C" __declspec(dllexport) void setForceMethod(void * voo, int forceVal);
extern "C" __declspec(dllexport) void setTreeRangeCoefficient(void * voo, double coef);
extern "C" __declspec(dllexport) void randomizeMassesNbodyModel(void * voo);
extern "C" __declspec(dllexport) void initializeNbodyModel(NbodyModel *theModel);
extern "C" __declspec(dllexport) void spinNbodyModel(NbodyModel *theModel);
extern "C" __declspec(dllexport) void speedNbodyModel(NbodyModel *theModel);

extern "C" __declspec(dllexport) double randRange(double low, double high);
extern "C" __declspec(dllexport) void setDefaultsNbodyModel(NbodyModel *theModel);
extern "C" __declspec(dllexport) void freeNbodyModel(NbodyModel *theModel);
extern "C" __declspec(dllexport) void setX(void * voo, double value, int i);
extern "C" __declspec(dllexport) void setXArray(void * voo, double * value);
extern "C" __declspec(dllexport) double getX(void * voo, int i);
extern "C" __declspec(dllexport) double * getXArray(void * voo);
extern "C" __declspec(dllexport) double computeSoftenedRadius(double g_m, double tstep_squared, double srad_factor);
extern "C" __declspec(dllexport) void stepNbodyModel(NbodyModel * theModel);

//force calculation methods
extern "C" __declspec(dllexport) void calcDerivsDirect(double * X, double * derivs, double t, double tStep,NbodyModel * theModel);
extern "C" _declspec(dllexport) void calcDerivsPPPM(double * x, double * derivs, double t, double tStep,NbodyModel * theModel);
extern "C" _declspec(dllexport) void calcDerivsBarnesHut(double * x, double * derivs, double t, double tStep,NbodyModel * theModel);
//integration methods
extern "C" __declspec(dllexport) void stepNbodyModelEuler(NbodyModel * theModel, double tStep);
extern "C" __declspec(dllexport) void stepNbodyModelIEuler(NbodyModel * theModel, double tStep);
extern "C" __declspec(dllexport) void stepNbodyModelMPEuler(NbodyModel * theModel, double tStep);
extern "C" __declspec(dllexport) void stepNbodyModelRK4(NbodyModel * theModel, double tStep);
extern "C" __declspec(dllexport) void stepNbodyModelLeapfrog(NbodyModel * theModel, double tStep);
extern "C" __declspec(dllexport) void stepNbodyModelABM(NbodyModel * theModel, double tStep);




